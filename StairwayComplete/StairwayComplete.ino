
const int relayPins[] = {0, 1, 2, 3, 4, 5, 6, 7, 11, 10, 9, 8, A5, A4, 13, 12};
const int pinsUsed = 16;
const int motionDown = A3;
const int motionUp = A2;
const int lightSensor = A1;
const int delayLights = 500;
const int delaySensorCheck = 250;
const int lightOnFlag = LOW; // Light on means "LOW"
long timeAllOn = (0.25 * 60 * 1000) -  pinsUsed * delayLights;

int motionDownStatus = LOW;
int motionUpStatus = LOW;
int lightStatus = LOW;
int activeRelay = 0;
int loopCounter = 0;

bool motionDetected() {
  return (motionDownStatus == HIGH || motionUpStatus == HIGH);
}

void delayWithSensorCheck(int delayTime) {
  delay(delayTime);
  motionDownStatus = digitalRead(motionDown);
  motionUpStatus = digitalRead(motionUp);
}

void waitTillNoMotion() {
  // While all lights are on, we need to check if someone
  // entered the stairway. In that case we reset the waiting
  // counter.
  const int maxIterations = timeAllOn / delaySensorCheck;
  int loopCounter = 0;

  while (loopCounter < maxIterations) {
    delayWithSensorCheck(delaySensorCheck);
    if (motionDetected()) {
      // Sensor triggert, reset loop counter
      Serial.println("Resetting");
      loopCounter = 0;
    }
    loopCounter++;
  }
}

void setup() {
  // put your setup code here, to run once:
  if (timeAllOn < 0) {
    timeAllOn = 0;
  }

  Serial.begin(9600);
  delay(5000);
  Serial.println("HelloWorld");
  Serial.println(timeAllOn);
  for (int i = 0; i < pinsUsed; ++i) {
    pinMode(relayPins[i], OUTPUT);
  }

  pinMode(A3, INPUT);
  pinMode(A2, INPUT);
  pinMode(A1, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  if (!motionDetected) {
    delay(delaySensorCheck);
  }  

  lightStatus = digitalRead(lightSensor);
  motionDownStatus = digitalRead(motionDown);
  motionUpStatus = digitalRead(motionUp);

  if (lightStatus != lightOnFlag) {
    // Its dark, lets light it up!
    if (motionDownStatus == HIGH)
    {
      // Sensor down triggert
      // Switch lights on
      for (int i = 0; i < pinsUsed; ++i) {
        digitalWrite(relayPins[i], HIGH);
        delay(delayLights);
      }

      // Keep the lights on
      waitTillNoMotion();

      // Switch lights off
      for (int i = 0; i < pinsUsed; ++i) {
        digitalWrite(relayPins[i], LOW);
        delayWithSensorCheck(delayLights);
        if (motionDetected()) {
          break;
        }
      }
    }
    if (motionUpStatus == HIGH)
    {
      // Sensor up triggert
      // Switch light on
      for (int i = pinsUsed - 1; i >= 0; --i) {
        digitalWrite(relayPins[i], HIGH);
        delay(delayLights);
      }

      // Keep the lights on
      waitTillNoMotion();

      for (int i = pinsUsed - 1; i >= 0; --i) {
        digitalWrite(relayPins[i], LOW);
        delayWithSensorCheck(delayLights);
        if (motionDetected()) {
          break;
        }
      }
    }
  }
}
